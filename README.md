# BeBeC 2018
### NOISE QUANTIFICATION WITH BEAMFORMING DECONVOLUTION: EFFECTS OF REGULARIZATION AND BOUNDARY CONDITIONS

This repository contains the code that accompanied the paper:

Oliver Lylloff and Efren Fernandez-Grande *"NOISE QUANTIFICATION WITH BEAMFORMING DECONVOLUTION: EFFECTS OF REGULARIZATION AND BOUNDARY CONDITIONS"*, Berlin Beamforming Conference, March 2018.

### Viewing code and figures

The figures are generated with this [notebook](https://nbviewer.jupyter.org/urls/gitlab.windenergy.dtu.dk/ollyl/BeBeC18/raw/master/BeBeC2018_figs.ipynb).

To view the code that generates the data for the figures, view [case I: Boundary conditions](https://nbviewer.jupyter.org/urls/gitlab.windenergy.dtu.dk/ollyl/BeBeC18/raw/master/Case_I_data.ipynb) or 
[case II: Regularization](https://nbviewer.jupyter.org/urls/gitlab.windenergy.dtu.dk/ollyl/BeBeC18/raw/master/Case_II_data.ipynb)

### Running the code 

*Note: Requires test case b7 from https://www.b-tu.de/fg-akustik/lehre/aktuelles/arraybenchmark to run full simulations (computational time ~ few hours). To run only the figure generation, download data files for [case I](https://files.dtu.dk/u/tYvFmWILxZyni8WZ/BeBeC2018_data_caseI.jld2?l) and [case II](https://files.dtu.dk/u/9G0CyiPtAszmbQLK/BeBeC2018_data_caseII.jld2?l). 

To run the code locally, follow the steps below to setup. 

* Download julia version 0.6.2 https://.julialang.org/downloads
* Start julia and run ```Pkg.init()``` and ```Pkg.update()```.
* Clone the [AeroAcoustics.jl](https://gitlab.windenergy.dtu.dk/ollyl/AeroAcoustics.jl) package in julia  ```Pkg.clone("https://gitlab.windenergy.dtu.dk/ollyl/AeroAcoustics.jl")```
* Locate the directory for the package with ```Pkg.dir()``` and quit julia.
* Go to AeroAcoustics.jl located in the package directory and checkout the BeBeC tag: ```git checkout BeBeC-2018```
* Clone this repository (or download zip), move downloaded data files to the BeBeC18 directory, and start julia. To view notebooks, add the [IJulia](https://github.com/JuliaLang/IJulia.jl) package:

```
Pkg.add("IJulia")
using IJulia
notebook()
```

A Jupyter notebook will now open in a browser window and the BeBeC notebooks can be run locally. 
